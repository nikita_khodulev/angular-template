'use strict';
var gulp = require('gulp');
var del = require('del');
var copy = require('gulp-copy');
var uglify = require('gulp-uglify');
var less = require('gulp-less');
var LessPluginCleanCSS = require('less-plugin-clean-css'),
    cleancss = new LessPluginCleanCSS({ advanced: true });
var sourcemaps = require('gulp-sourcemaps');
var rjs = require('gulp-requirejs');
var nodemon = require('gulp-nodemon');
var htmlmin = require('gulp-htmlmin');
var rename = require('gulp-rename');
var gulpNgConfig = require('gulp-ng-config');
var argv = require('yargs').argv;


var base = {
    app: 'app/',
    dist: 'dist/'
};

var pathes = {
    assets: ['styles.css', 'assets/{fonts,icons,img}/*'],
    css: ['./app/**/*.{css,less}', '!./app/styles.css'],
    html: ['./app/**/*.html', '!./app/bower_components/**'],
    less: './app/assets/styles/styles.less',
    requirejs_main: 'main.js',
    envConfig: 'env_config.json',
    server: 'devServ.js'
};



gulp.task('minify:js', function () {
    return rjs({
        name: 'main',
        baseUrl: base.app,
        out: pathes.requirejs_main,
        mainConfigFile: base.app + pathes.requirejs_main
    })
        .pipe(uglify({ mangle: false }))
        .pipe(gulp.dest(base.dist));
});


gulp.task('less', function () {
    return gulp.src(pathes.less)
        .pipe(sourcemaps.init())
        .pipe(less({
            plugins: [cleancss]
        }))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest(base.app));
});


gulp.task('watch', function () {
    // Watch all the styles files, then run the less task
    gulp.watch(pathes.css, ['less']);
});


gulp.task('default', ['config', 'less', 'watch'], function () {
    nodemon({
        script: pathes.server
    })
});


gulp.task('clean:dist', function () {
    return del(base.dist);
});


gulp.task('minify:html', function () {
    return gulp.src(pathes.html)
        .pipe(htmlmin({collapseWhitespace: true}))
        .pipe(gulp.dest(base.dist))
});


gulp.task('copy-assets', function () {
    return gulp.src(pathes.assets, {cwd: base.app})
        .pipe(copy(base.dist));
});

function make_config_file(env, cb){
    var wrapTemplate = "define(['angular'], function () {\n'use strict';\n\nreturn <%= module %> \n});";

    gulp.src(pathes.envConfig)
        .pipe(gulpNgConfig('envConfig', {
            environment: env,
            wrap: wrapTemplate
        }))
        .pipe(rename('config.js'))
        .pipe(gulp.dest('./app/assets/scripts'))
        .on('end', function() { cb();});
}

gulp.task('config', function(cb) {
    var env;
    if (argv.production === true){
        env = "production";
    } else if (argv.dev === true) {
        env = "dev";
    } else {
        env = "staging";
    }
    make_config_file(env, cb);
});

gulp.task('release', ['clean:dist', 'config', 'less'], function () {
    gulp.start('minify:html', 'minify:js', 'copy-assets');
});