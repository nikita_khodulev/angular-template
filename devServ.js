var express = require('express');
var morgan = require('morgan');
var livereload = require('livereload');

var app = express();

app.use(morgan('dev'));
app.use(express.static('./app/'));

app.listen(3000, function () {
  console.log('Radaro-front emulator listening on port 3000!');
});

server = livereload.createServer();
server.watch([__dirname + "/app", "!" + __dirname + "/app/bower_components"]);
