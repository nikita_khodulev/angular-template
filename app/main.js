'use strict';

require.config({
    paths: {
        'jquery': 'bower_components/jquery/dist/jquery.min',
        'angular-loader': 'bower_components/angular-loader/angular-loader.min',
        'angular': 'bower_components/angular/angular',
        'uiRouter': 'bower_components/angular-ui-router/release/angular-ui-router.min',
        'ngAnimate': 'bower_components/angular-animate/angular-animate.min',
        'ui.bootstrap': 'bower_components/angular-bootstrap/ui-bootstrap-tpls.min',
        'ngAria': 'bower_components/angular-aria/angular-aria.min',
        'ngResource': 'bower_components/angular-resource/angular-resource.min',
        'ngSanitize': 'bower_components/angular-sanitize/angular-sanitize.min',
        'lodash': 'bower_components/lodash/dist/lodash.min',
        'ngCookies': 'bower_components/angular-cookies/angular-cookies.min',
        'ngStorage': 'bower_components/ngstorage/ngStorage.min',
        'app': './assets/scripts/app',
        'routes': './assets/scripts/routes'
    },
    shim: {
        'angular': {exports: 'angular', deps: ['jquery']},
        'uiRouter': {deps: ['angular']},
        'ngAnimate': {deps: ['angular']},
        'ui.bootstrap': {deps: ['angular']},
        'ngAria': {deps: ['angular']},
        'ngResource': {deps: ['angular']},
        'ngSanitize': {deps: ['angular']},
        'lodash': {deps: ['angular']},
        'ngCookies': {deps: ['angular']},
        'ngStorage': {deps: ['angular']},
        'app': {deps: ['angular']}
    }
});

require([
    'require',
    'angular',
    'app',
    'routes'

], function(require, ng) {
    'use strict';

    ng.element(document).ready(function () {
        ng.bootstrap(document, ['app']);
    })
});