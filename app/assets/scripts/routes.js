define(['app'], function (app) {
	'use strict';

	 app.config(['$stateProvider', '$urlRouterProvider',function($stateProvider, $urlRouterProvider){
		$stateProvider
			.state('main', {
				url: '^/',
				templateUrl: 'main/main.html',
				controller: 'MainCtrl'
			})
	}]);

	return app;

});