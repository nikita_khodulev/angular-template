define([
	'angular',
	'lodash',
	'uiRouter',
	'ngAnimate',
	'ui.bootstrap',
	'ngAria',
	'ngResource',
	'ngSanitize',
	'ngCookies',
	'ngStorage',
	'./main/mainController'

], function (ng) {
	'use strict';

	var app = ng.module('app', [
		'ui.router',
		'ui.bootstrap',
		'ngAnimate',
		'ngCookies',
		'ngStorage',
		'mainController'
		]);

	return app;
});